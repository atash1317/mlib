<!doctype html>
<?php
require('mlib_values.php');
# CSCI59P standard functions
function html_head($title) {
  echo '<html lang="en">';
  echo '<head>';
  echo '<meta charset="utf-8">';
  echo "<title>$title</title>";
  echo '<link rel="stylesheet" href="mlib.css">';
  echo '</head>';
  echo '<body>';
}

function try_again($str) {
  echo $str;
  echo "<br/>";
  //the following emulates pressing the back button on a browser
  echo '<a href="#" onclick="history.back(); return false;">Try Again</a>';
  require('mlib_footer.php');
  exit;
}

function validate_media($title, $author, $type, $description) {
  $error_messages = array(); # Create empty error_messages array.
  if ( strlen($title)  == 0 ) {
    array_push($error_messages, "Title field must have a media title.");
  }

  if ( strlen($author) == 0 ) {
    array_push($error_messages, "Author field must have a author name.");
  }

  if ( strlen($type) == 0 ) {
    array_push($error_messages, "type field must have a media type.");
  }

  if ( strlen($description) == 0 ) {
    array_push($error_messages, "Description field must have a media description.");
  }

  try {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //check for invalid media type
    if ( strlen($type) != 0 ) {
      $sql = "SELECT COUNT(*) FROM mlib_types WHERE type = '$type' AND status = 'active'";
      $result = $db->query($sql)->fetch(); //count the number of entries with the media name
      if ( $result[0] == 0) {
        array_push($error_messages, "Media type $type is not defined. Media type must be valid.");
      }
    }

    //check for duplicate media name
    if ( strlen($title) != 0 ) {
      $sql = "SELECT COUNT(*) FROM media WHERE title = '$title' AND status = 'active'";
      $result = $db->query($sql)->fetch(); //count the number of entries with the media title
      if ( $result[0] > 0) {
        array_push($error_messages, "$title is not unique. Title must be unique.");
      }
    }
  }

  catch(PDOException $e){
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }

  return $error_messages;
}

// Validate the date format as YYYY-MM-DD
function MyCheckDate( $postedDate ) {
   if (preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $postedDate, $datebit)) {
      return checkdate($datebit[2] , $datebit[3] , $datebit[1]);
   } else {
      return false;
   }
}

// Check to see if we are logged in as an admin
function we_are_not_admin()
{
  if (empty($_SESSION['valid_user'])) {
    echo "Only administrators can execute this function.<br/>";
    require('teqco_footer.php');
  	return true;
	}
}
?>
