<!doctype html>
<?php
require('mlib_values.php');
require('mlib_functions.php');
html_head("mlib Media");
require('mlib_header.php');
session_start();
require('mlib_sidebar.php');

if (we_are_not_admin()) {
  exit;
}

# Code for your web page follows.
if (!isset($_POST['submit']))
{
?>
<!-- Display a form to capture information -->
<h2>Add Media</h2>
<form action="mlib_media.php" method="post">
  <table border="0">
    <tr bgcolor="#cccccc">
      <td width="100">Field</td>
      <td width="300">Value</td>
    </tr>
    <tr>
      <td>Title</td>
      <td align="left"><input type="text" name="title" size="35" maxlength="35"></td>
    </tr>
    <tr>
      <td>Author</td>
      <td align="left"><input type="text" name="author" size="35" maxlength="35"></td>
    </tr>
    <tr>
      <td>Description</td>
      <td align="left"><input type="text" name="description" size="70" maxlength="350"></td>
    </tr>
    <tr>
      <td>Type</td>
      <td align="left">
    <select name="type">
<?php
  // Replace text field with a select pull down menu.
  try {
    //open the database
    $db = new PDO(DB_PATH,DB_LOGIN,DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //display all types in the types table
    $result = $db->query('SELECT * FROM mlib_types');
    foreach($result as $row) {
      print "<option value=".$row['type'].">".$row['type']."</option>";
    }

    // close the database connection
    $db = NULL;
  }

  catch(PDOException $e) {
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
?>
    </select>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="right">
        <input type="submit" name="submit" value="Submit"></td>
    </tr>
  </table>
</form>
<?php
} else {
  # Process the information from the form displayed
  $title = $_POST['title'];
  $author = $_POST['author'];
  $description = $_POST['description'];
  $type = $_POST['type'];

  //clean up and validate data
  $title = trim($title);
  $author = trim($author);
  $description = trim($description);
  $type = trim($type);

$errors = validate_media($title, $author, $type, $description);
if(empty($errors)){
  try{
    //open the database
    $db = new PDO(DB_PATH,DB_LOGIN,DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //insert data...
    $db->exec("INSERT INTO media (title, author, type, status, user_id, description) VALUES ('$title','$author', '$type', 'active', 0, '$description');");

    //get the last id value inserted into the table
    $last_id = $db->lastInsertId();

    //now output the data from the insert to a simple html table...
    print "<h2>Media Added</h2>";
    print "<table border=1>";
    print "<tr>";
    print "<td>Title</td>";
    print "<td>Author</td>";
    print "<td>Description</td>";
    print "<td>Type</td>";
    print "</tr>";
    $row = $db->query("SELECT * FROM media where id = '$last_id'")->fetch(PDO::FETCH_ASSOC);
    print "<tr>";
    print "<td>".$row['title']."</td>";
    print "<td>".$row['author']."</td>";
    print "<td>".$row['description']."</td>";
    print "<td>".$row['type']."</td>";
    print "</tr>";
    print "</table>";

    // close the database connection
    $db = NULL;
  }

  catch(PDOException $e){
    echo 'Exception : '.$e->getMessage();
    echo "<br/>";
    $db = NULL;
  }
  }else {
      echo "Errors found in media entry:<br/>";
      foreach($errors as $error) {
        echo " -  $error <br/>";
      }
      try_again("Please correct.<br/>");
  }
}
require('mlib_footer.php');
?>
