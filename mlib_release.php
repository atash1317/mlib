<!doctype html> <?php require('mlib_functions.php'); html_head("mlib release"); 
require('mlib_header.php'); require('mlib_sidebar.php');
# Code for your web page follows.
if (!isset($_POST['submit'])) {
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); ?>
    <h2>Release Media</h2>
    <form action="mlib_release.php" method="post">
      <!-- display checked out media -->
      <table border=1>
        <tr>
        	<td>Click to 
Release</td><td>title</td><td>Type</td><td>Description</td><td>User</td><td>Reserved 
Till</td>
        </tr>
        
<?php
    $result = $db->query("SELECT * FROM media WHERE status = 'active' AND user_id > 0 
ORDER by title");
    foreach($result as $row)
    {
      print "<tr>";
      print "<td><input type='checkbox' name='id[]' value=".$row['id']."></td>";
      print "<td>".$row['title']."</td>";
      print "<td>".$row['type']."</td>";
      print "<td>".$row['description']."</td>";
      $user_id = $row['user_id'];
      $result = $db->query("SELECT * FROM mlib_users WHERE id = $user_id")->fetch();
      $user_name = $result['first']." ".$result['last'];
      print "<td>".$user_name."</td>";
      print "<td>".$row['date_in']."</td>";
      print "</tr>";
    }
?>
      </table>
      <input type="submit" name="submit" value = "Submit"/><br/>
    </form> <?php
    
    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
} else {
?>
    <h2>Media Released</h2> <?php
  $id = $_POST['id'];
  try
  {
    //open the database
    $db = new PDO(DB_PATH, DB_LOGIN, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $n = count($id);
    if ($n == 0) {
      echo "You did not select any items to release.<br/>";
    } else {
      //update each piece of media with user_id = 0
      for($i=0; $i < $n; $i++)
      {
        $db->exec("UPDATE media SET user_id = 0, date_in = null WHERE id = $id[$i]");
      }
      //now output the data to a simple html table...
      print "<table border=1>";
      print "<tr>";
      print "<td>title</td><td>Type</td><td>Description</td>";
      print "</tr>";
      for($i=0; $i < $n; $i++)
      {
        $sql = "SELECT * FROM media WHERE id = $id[$i]";
        $row = $db->query($sql)->fetch(PDO::FETCH_ASSOC);
        print "<tr>";
        print "<td>".$row['title']."</td>";
        print "<td>".$row['type']."</td>";
        print "<td>".$row['description']."</td>";
        print "</tr>";
      }
      print "</table>";
    }
    // close the database connection
    $db = NULL;
  }
  catch(PDOException $e)
  {
    echo 'Exception : '.$e->getMessage().'<br/>';
    $db = NULL;
  }
}
require('mlib_footer.php');

 ?>
