<aside id="sidebar">
  <nav>
    <ul>
      <li>
        <a href="mlib_status.php">Media Status</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_reserve.php">Reserve Media</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_release.php">Release Media</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_login.php">Login as Administrator</a>
      </li>
    </ul>
<?php
  if (!empty($_SESSION['valid_user'])) {
?>
  <hr/>
    <ul>
      <li>
        <a href="mlib_media.php">Add Media</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_users.php">Add Users</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_upload.php">Upload Media List</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_administrator.php">Administrator Config</a>
      </li>
    </ul>
  <hr/>
    <ul>
      <li>
        <a href="mlib_logout.php">Log Out</a>
      </li>
<?php
  }
?>
    </ul>
  </nav>
</aside>
